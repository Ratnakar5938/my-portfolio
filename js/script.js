
// about section tabs

(() => {
const aboutSection = document.querySelector(".about-section"),
tabsContainer = document.querySelector(".about-tabs");

tabsContainer.addEventListener("click", (event)=>{
    if(event.target.classList.contains("tab-item") && !event.target.classList.contains("active")){
       const target = event.target.getAttribute("data-target");
    // deactivate existing active 'tab=item'
    tabsContainer.querySelector(".active").classList.remove("outer-shadow", "active");
    // activating the clicked 'tab-item'
    event.target.classList.add("outer-shadow", "active");
    // deactivate existing 'tab-content'
    aboutSection.querySelector(".tab-content.active").classList.remove("active");
    // activating the clicked 'tab-content'
    aboutSection.querySelector(target).classList.add("active");
    }
})
})();